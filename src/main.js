import React, {Component} from 'react';
import { Switch, Route } from 'react-router-dom'


import Home from './components/home';
import Intro from './components/intro';
import Details from './components/details';

class Main extends Component{
    render(){
        return(
            <main>
                <Switch>
                    <Route exact path='/' component={Intro} />
                    <Route exact path='/home' component={Home} />
                    <Route exact path='/details' component={Details} />
                </Switch>
            </main>
        )
    }
}

export default Main;

