import React, { Component } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as starQuizActions from '../actions/starQuiz';

import { Link } from 'react-router-dom';        
import TextField from 'material-ui/TextField';
import { FormControl } from 'material-ui/Form';
import Button  from 'material-ui/Button';


const styles = {
    position: 'absolute',
    height: '92%',
    // backgroundImage: `url(${background})`,
    color: '#fff',
    padding: '15px'
}

class Intro extends Component{
    constructor(){
        super()
        this.state = {
            name: null,
            species: null,
            height: null
        }
    }

    handleChange = (e) => {
        this.props.addUser({
            [e.target.name]: e.target.value
        })
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    // prepareContent = () => {
    //     let r = [];
    //     getPeople()
    //     .then(res => {
    //         r = res.data;
    //         let characters = []
    //         r.results.forEach((e, i) => {
    //             e = Object.assign(e, {id: i})
    //             characters.push(e)
    //         });
    //         this.props.getCharacters(characters)
    //     })
    // }
    render(){
        const nextPage = this.state.name && this.state.species && this.state.height ?
        (
        <div>
            <p>Que a força esteja com você!</p>
              <Button onClick={this.prepareContent} component={Link} to="/home">
                Avançar
            </Button>
             
        </div>
        )
        : <p>Preencha o cadastro</p>
        return(
            <section style={styles} className='second-bg'>
                <div xs={3}>
                    <p>
                        Em uma galáxia tão, tão distante, havia um jovem padawan
                        que sonhava em ser um mestre jedi do React, enquanto treinava e 
                        aprimorava as suas habilidades programísticas, o império, contra-atacava. Tá parei!
                    </p>
                    
                    <h3>Quem é você?</h3>
                    <FormControl fullWidth>
                        <TextField
                            label='Digite seu nome'
                            margin='normal'
                            name='name'
                            onChange={this.handleChange}
                        />
                    </FormControl>
                    <FormControl fullWidth>
                        <TextField
                            label='Sua espécie'
                            margin='normal'
                            name='species'
                            onChange={this.handleChange}
                        />  
                    </FormControl>
                    <FormControl fullWidth>
                        <TextField
                            label='a sua altura em cm'
                            margin='normal'
                            name='height'
                            type='number'
                            onChange={this.handleChange}
                        />  
                    </FormControl>
                    {nextPage}
              </div>
            </section>
     
        )
    }
}
const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = dispatch => 
    bindActionCreators(starQuizActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Intro);
