import React, { Component } from 'react';
import { Link } from 'react-router-dom';        

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as starQuizActions from '../actions/starQuiz';

import { getPeople } from '../utils/apiCalls';

import List, { ListItem, ListItemText } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Button from 'material-ui/Button';
import load from '../images/loading.gif'


class Home extends Component{
    constructor(){
        super()
        this.state = {
            count: null,
            next: 'https://swapi.co/api/people/',
            previous: null,
            characters: [],
            loading: true
        }
        
    }
    componentWillMount = () =>{
        this.getCharacters(this.state.next)

    }

    getCharacters = () => {
        getPeople(this.state.next)
        .then((r) => {
            let characters = []
            r.data.results.forEach((e, i) => {
                e = Object.assign(e, {id: i})
                characters.push(e)
            });
          
            let state = {
                count: r.data.count,
                next: r.data.next,
                previous: r.data.previous
            }
            this.props.getCharacters(characters)
            this.props.page(r.data.next)
            console.log(this.props)
            this.setState(state, () => {
                this.setState({loading: false})
            });
        })
    }

    setDetails = (person) => {

        this.props.getDetails(person)

    }
    render(){
        const showMore = this.state.next ?
        <Button className="btn-loading" onClick={this.getCharacters}>Mostrar mais</Button>
        : null

        const content = this.state.loading ? (
            <div>               

                <img alt="loading" className="loading" src={load} />
                <p className='alignCenter'>Carregando...</p>
            </div>
 
         ) : (
             <div>
         <List>
             {
                 this.props.characters ? 
                 this.props.characters.map((e, i) => {
                    // <ListItem key={i} component={Link} to={{
                    //     pathname: "/details",
                    //     id: e.id
                    // }}>
                    return  <ListItem key={i} component={Link} onClick={() => this.setDetails(e)} to='/details'>

                                <Avatar>
                                    <img alt="avatar" className='avatar' src={require(`../images/${e.id}.jpg`)} />
                                </Avatar>
                                <ListItemText key={e.id} primary={e.name} secondary={e.gender} />
                            </ListItem>
                    }) : null
             }
         </List>
                 {showMore}
        </div>
        )
    

        return(
            <section className='second-bg'>

                {content}
            
            </section>
        )
    }
}

const mapStateToProps = state => ({
    characters: state.characters,
    page: state.page,
    user: state.user
});

const mapDispatchToProps = dispatch => 
    bindActionCreators(starQuizActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);

