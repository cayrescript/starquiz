import React, { Component } from 'react';

//REDUX
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as starQuizActions from '../actions/starQuiz';


import PropTypes from 'prop-types';
import List, { ListItem, ListItemText } from 'material-ui/List';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import Typography from 'material-ui/Typography';

import { getPlanet } from '../utils/apiCalls';
import { episodeId } from '../utils/string';

function TabContainer(props) {
    return (
      <Typography component="div" style={{ padding: 8 * 3 }}>
        {props.children}
      </Typography>
    );
  }

  const Avatar = ({id}) => {
    return <img alt="avatar" className='avatar' src={require(`../images/${id}.jpg`)} /> 
  } 
  const Films = ({films}) => {
    return (
      <div className="carousel">
          {films.map(e => {
            let id = episodeId(e);
              return <img alt="img-movie" className='a' src={require(`../images/films/${id}.jpg`)} />                      
            })
          }
      </div>
    )
  }
  // const InfoPerfil = ({person}) => {
  //   // let teste = person.map(e => e);
  //   let perfil = {
  //     name: person.name,
  //     gender: person.gender,
  //     hair_color: person.hair_color,
  //     skin_color: person.skin_color,
  //     eye_color: person.eye_color,
  //     birth_year: person.birth_year
  //   }
  //   return (<List>
  //     {perfil
  //     .map(e => <ListItem ><ListItemText primary={e} /> {console.log(e)}</ListItem>)}
  //     </List>)
  //   console.log(person)
  // }

  TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
  };



class Details extends Component {
    constructor(){
        super()
        this.state = {
            value: 0,
          };
    }

    componentWillMount(){
      
      getPlanet(this.props.details.homeworld)
      .then(e => {
          let details = this.props.details
          details['planet'] = e.data
          this.props.getDetails({planet: e.data})
        })

    }
    
      handleChange = (event, value) => {
        this.setState({ value });
      };
    
      handleChangeIndex = index => {
        this.setState({ value: index });
      };
    render() {
        const { details } = this.props;
        return(
            <div>
              <Avatar id={this.props.details.id}/>

            <AppBar position="static" color="default" className="primary gold-color">
              <Tabs
                value={this.state.value}
                onChange={this.handleChange}
                indicatorColor="primary"
                fullWidth
              >
                <Tab label="Perfil" />
                <Tab label="Planeta" />
                <Tab label="Filmes" />
              </Tabs>
            </AppBar>
            {this.Perfil}
            {this.state.value === 0 && <TabContainer>
              <List>
                  <span>Nome </span>
                 <ListItem ><ListItemText primary={details.name} /></ListItem>
                 <span>Gênero </span>
                 <ListItem ><ListItemText primary={details.gender} /></ListItem>
                 <span>Cor do cabelo </span>
                 <ListItem ><ListItemText primary={details.hair_color} /></ListItem>
                 <span>Cor da pele </span>
                 <ListItem ><ListItemText primary={details.skin_color} /></ListItem>
                 <span>Cor dos olhos </span>
                 <ListItem ><ListItemText primary={details.eye_color} /></ListItem>
                 <span>Ano de nascimento</span>
                 <ListItem ><ListItemText primary={details.birth_year} /></ListItem>
              </List>
                
            </TabContainer>}
            {this.state.value === 1 && <TabContainer>
              {console.log(details.planet)}
              <List>
                  <span>Nome </span>
                 {/* <ListItem ><ListItemText primary={details.planet.name} /></ListItem> */}
                 <span>População </span>
                 <ListItem ><ListItemText primary={details.planet.population} /></ListItem>
                 <span>Água da superfície </span>
                 <ListItem ><ListItemText primary={details.planet.surface_water} /></ListItem>
                 <span>Período de rotação </span>
                 <ListItem ><ListItemText primary={details.planet.rotation_period} /></ListItem>
                 <span>Período orbital </span>
                 <ListItem ><ListItemText primary={details.planet.orbital_period} /></ListItem>
                 <span>Diâmetro</span>
                 <ListItem ><ListItemText primary={details.planet.diameter} /></ListItem>
                 <span>Clima</span>
                 <ListItem ><ListItemText primary={details.planet.climate} /></ListItem>
                 <span>Gravidade</span>
                 <ListItem ><ListItemText primary={details.planet.gravity} /></ListItem>
                 <span>Terreno</span>
                 <ListItem ><ListItemText primary={details.planet.terrain} /></ListItem>
              </List>
            </TabContainer>}
            {this.state.value === 2 && <TabContainer fullWidth>
              <Films films={details.films}/>
            </TabContainer>}
          </div>
        )
    }
}

const mapStateToProps = state => ({
    characters: state.characters,
    user: state.user,
    details: state.details
});

const mapDispatchToProps = dispatch => 
    bindActionCreators(starQuizActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Details);

