import React, { Component } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as starQuizActions from '../actions/starQuiz';
import store from '../store';


import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import AccountCircle from 'material-ui-icons/AccountCircle';
import Chip from 'material-ui/Chip';
import Button from 'material-ui/Button';
import { Link } from 'react-router-dom';        


const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

class Head extends Component {
  constructor(props){
    super(props)
    this.state = {
     
    }
    console.log(this.props)
  }
  componentWillReceiveProps(nextProps) {
    this.setState({user: store.getState().user})
    console.log(this.state)
  }

  render() {

    const { classes } = this.props;

    return (
      <div className={classes.root}>

        <AppBar position="static" className="primary">
          <Toolbar>
            <Typography variant="title" color="inherit" className={classes.flex}>
              StarQuiz
            </Typography>
              
                <div>
                {this.state.user ? 
                  <Chip
                    avatar={  <AccountCircle />}
                    label={this.state.user.name}
                  /> : <Button component={Link} to="/">Cadastrar</Button> 
                }
              </div>
          
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

Head.propTypes = {
  classes: PropTypes.object.isRequired,
};


const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = dispatch => 
    bindActionCreators(starQuizActions, dispatch);

export default(connect(mapStateToProps, mapDispatchToProps), withStyles(styles)(Head));