import axios from 'axios';

export function getPeople(endpoint) {
   return axios.get(endpoint)
}

export function getPlanet(endpoint){
    return axios.get(endpoint)
}