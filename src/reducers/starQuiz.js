export function user(state = [], action) {
    switch(action.type){
        case 'ADD_USER': 
            let user = {...state}
            user[Object.keys(action.user)] = action.user[Object.keys(action.user)]
            return user
        default:
            return state;
    } 
}
export function characters(state = [], action) {
    switch(action.type){
        case 'GET_CHAR':
            let s = [...state];
                action.characters.forEach(e => {
                    s.push(e)
                });
            return s
        default:
            return state;
    }
}
export function page(state = '', action){
    switch(action.type){
        case 'SET_PAGE':
            return action.page
        default: 
            return state
    }
}

export function details(state = {}, action){
    switch(action.type){
        case 'GET_DETAILS':
            const details = action.details;
             return Object.assign({}, state, details)
        default: 
            return state
    }
}
