import { combineReducers } from 'redux';

import {user, characters, page, details} from './starQuiz';

export default combineReducers({
    user, characters, page, details
})