export function addUser(user) {
    return {
        type: 'ADD_USER',
        user: {
            [Object.keys(user)]: user[Object.keys(user)]
        }
    }
}
export function getCharacters(char) {
    return {
        type: 'GET_CHAR',
        characters: char
    }
}
export function page(page) {
    return {
        type: 'SET_PAGE',
        page: page
    }
}
export function getDetails(details){
    return  {
        type: 'GET_DETAILS',
        details: details
    }
}