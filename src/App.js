import React, { Component } from 'react';

import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom'
import store from './store';

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import './index.css';

import Head from './components/head';
import Main from './main';

const theme = createMuiTheme({
  palette: {
    type: 'dark', 
  },
});


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MuiThemeProvider theme={theme}>
          <BrowserRouter>
            <div>
              <Head />
              <Main />
            </div>
          </BrowserRouter>
        </MuiThemeProvider>
      </Provider>
  
    );
  }
}

export default App;
