#Lista de to-do: 


Features:
Search
Filters
    loading na paginação: Usuário não tem feedback ao clicar no botão "mostrar mais"
    Melhorias de layout Desktop
Bugfixes:
    Página Details: Em alguns momentos o componente é renderizado antes de receber o estado atual, causando um erro.
    Ocultar botão de cadastro na página inicial
    Corrigir iteração dos filmes na página Details
Refactor:
    Apenas um componente por arquivo
    Organizar estrutura do projeto
    Abstrair funções